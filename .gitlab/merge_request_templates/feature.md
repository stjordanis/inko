<!--
This template contains various comments (like this). These should be replaced
with the content they ask for (e.g. a summary). Once done, make sure all are
removed, including this one.
-->

## Details

<!-- Provide a detailed description of this merge request here. -->

## Corresponding issue

<!-- Add a link to the issue describing the implemented feature. -->

## Checklist

* [ ] Added tests
* [ ] Added documentation
* [ ] Inko source code follows the [Inko style guide](https://inko-lang.org/manual/style-guide/)

/label ~type::Feature
