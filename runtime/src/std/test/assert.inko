# Assertions for writing unit tests.
#
# Assertions can be used to define requirements that must be met for a unit
# test to pass. For example, the `equal` method can be used to assert that two
# values must be equal to each other.
#
# Assertions will throw in the event of a failure, requiring the use of the
# `try` keyword. The use of `try!` is discouraged as it will result in the
# entire test suite terminating in the event of an assertion failing.
import std::conditional::Conditional
import std::format::Inspect
import std::operators::(Equal, Greater)
import std::process::(self, Process)
import std::string_buffer::StringBuffer

# The result of a PanicTest.
object PanicResult {
  # A `Boolean` that indicates if a block panicked or not.
  @panicked: Boolean

  # A `String` containing the panic error message, if a panic happened.
  @error: ?String

  def init(panicked: Boolean, error: ?String = Nil) {
    @panicked = panicked
    @error = error
  }

  def panicked? -> Boolean {
    @panicked
  }

  def error -> ?String {
     @error
  }
}

# A test to verify if a block will panic or not.
object PanicTest {
  # The process that owns the test.
  @owner: Process

  # The `Block` to run.
  @block: lambda

  def init(owner: Process, block: lambda) {
    @owner = owner
    @block = block
  }

  def run {
    process.panicking do (error) {
      notify_owner(PanicResult.new(panicked: True, error: error))
    }

    @block.call

    notify_owner(PanicResult.new(panicked: False))
  }

  def notify_owner(result: PanicResult) {
    @owner.send(result)
  }
}

# Asserts that the given arguments are equal to each other.
def equal!(T: Inspect + Equal)(given: T, expected: T) {
  (given == expected).if_true {
    return
  }

  let error = StringBuffer.new(
    'Expected ',
    given.inspect,
    ' to equal ',
    expected.inspect,
  )

  process.panic(error)
}

# Asserts that the given arguments are not equal to each other.
def not_equal!(T: Inspect + Equal)(given: T, expected: T) {
  (given == expected).if_false {
    return
  }

  let error = StringBuffer.new(
    'Expected ',
    given.inspect,
    ' not to equal ',
    expected.inspect,
  )

  process.panic(error)
}

# Asserts that the first argument is greater than the second argument.
def greater!(T: Inspect + Greater)(given: T, minimum: T) {
  (given > minimum).if_true {
    return
  }

  let error = StringBuffer.new(
    'Expected ',
    given.inspect,
    ' to be greater than ',
    minimum.inspect,
  )

  process.panic(error)
}

# Asserts that the given lambda panics.
def panic(block: lambda) {
  let proc = process.spawn {
    let test = process.receive as PanicTest

    test.run
  }

  proc.send(PanicTest.new(owner: process.current, block: block))

  let result = process.receive as PanicResult

  result.panicked?.if_true {
    return
  }

  process.panic('The provided block did not panic')
}

# Asserts that the given lambda does not panic.
def no_panic(block: lambda) {
  let proc = process.spawn {
    let test = process.receive as PanicTest

    test.run
  }

  proc.send(PanicTest.new(owner: process.current, block: block))

  let result = process.receive as PanicResult

  result.panicked?.if_false {
    return
  }

  process.panic('The provided block panicked: ' + result.error.to_string)
}

# Asserts that the given value is something that evaluates to be truthy.
#
# Something is considered to be truthy when its implementation of `if_true`
# calls the supplied block.
def true!(T: Inspect + Conditional)(value: T) {
  value.if_true {
    return
  }

  let error = StringBuffer.new('Expected ', value.inspect, ' to be truthy')

  process.panic(error)
}

# Asserts that the given value is something that evaluates to be falsy.
#
# Something is considered to be falsy when its implementation of `if_false`
# calls the supplied block.
def false!(T: Inspect + Conditional)(value: T) {
  value.if_false {
    return
  }

  let error = StringBuffer.new('Expected ', value.inspect, ' to be falsy')

  process.panic(error)
}
